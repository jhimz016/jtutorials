package jhimz;

public interface NetworkConnection {
	
	String connect(String connName);
	
	boolean connectionStatus();
	
}

