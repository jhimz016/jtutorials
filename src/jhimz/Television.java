package jhimz;

public class Television {

	public String brandName;
	public String modelName;
	public boolean powerOn = true;
	public int channel = 0;
	public int volume = 0;
	
	
	public void turnOn(){
		powerOn = true;
	}
	
	public void turnOff(){
		powerOn = false;
	}
	
	public void channelUp(){
		if (channel <= 10){
			channel = channel + 1;
		}
		
	}
	
	public void channelDown(){
		if (channel >= 0){
			channel = channel - 1;
		}
	}
	
	public void volumeUp(){
		if (volume <=5){
			volume++;
		}
	}
	
	public void volumeDown(){
		if (volume >= 0){
			volume--;
		}
	}
	
    public String toString(){
	  String var;
	  
	  var = brandName + " " + modelName + "[on:" + powerOn + ", channel:" + channel + ", volume:" + volume + "]";
	  return var;
	}

	public static void main(String[] args) {
		Television newTV = new Television();
		
		newTV.brandName = "Andre Electronics";
		newTV.modelName = "One";
		newTV.volume = 5;
		newTV.channel = 0;
		System.out.println(newTV.toString());
		
		if (newTV.powerOn){
				newTV.turnOff();
		}
		else { 
				newTV.turnOn();
		}
		
		for (int x=0; x < 5; x++){
			newTV.channelUp();
		}
		
		newTV.channelDown();
		
		for (int y=0; y < 3; y++){
			newTV.volumeDown();
		}
		
		newTV.volumeUp();
		
		if (newTV.powerOn){
			newTV.turnOn();
		}
		else {
			newTV.turnOff();
			
		}
		System.out.println(newTV.toString());
	}
}



