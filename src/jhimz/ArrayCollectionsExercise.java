package jhimz;

import java.util.*;
import java.util.Map.Entry;

public class ArrayCollectionsExercise{
	

	public static void main(String[] args) {
	
		String arrCountries[]= {"USA", "United Kingdom", "South Korea", "Japan", "France", "Netherlands", "Brazil", "UAE", "Greece", "Australia"};
		
		System.out.println("The size of the array of Countries is " + arrCountries.length);
		
		ArrayList<String> lstCountries = new ArrayList<String>();
		
				
				
		for (int x = 0; x < arrCountries.length; x++){
			lstCountries.add(arrCountries[x]);		
		}
		
		System.out.println("Old size of the List of Country is " + lstCountries.size());
		
		
		lstCountries.add("Scotland");
		
		if (lstCountries.isEmpty() == true){
			System.out.println("List of Country is Empty!");
		}
		else{
			System.out.println("List of Country is not Empty!");
		}
		
				
		System.out.println("New size of the List of Country is " + lstCountries.size());
		
		if (lstCountries.contains("Mongolia")){
			System.out.println("Mongolia is in the List!");
		}
		else{
			System.out.println("Mongolia is not in the List!");
		}
		
		if (lstCountries.contains("Scotland")){
			System.out.println("Scotland is in the List!");
		}
		else{
			System.out.println("Scotland is not in the List!");
		}

		lstCountries.remove(10);

		System.out.println("My list contains the countries from Trip Advisor's top 5 destination which are:");
		
		for (int y = 0; y < lstCountries.size(); y++){
			switch (lstCountries.get(y)){
			case "Indonesia": System.out.print(lstCountries.get(y) + ", "); break;
			case "United Kingdom": System.out.print(lstCountries.get(y) + ", "); break;
			case "France": System.out.print(lstCountries.get(y) + ", "); break;
			case "Italy": System.out.print(lstCountries.get(y) + ", "); break;
			case "USA": System.out.print(lstCountries.get(y) + ", "); break;
			}
		}
		System.out.println();
		
		System.out.println(lstCountries);
		
		Map<Integer, String> countryMap = new HashMap<Integer, String>();
		
		for (int y = 0; y < lstCountries.size(); y++){
			countryMap.put(y, lstCountries.get(y));
		}
		
		for (Entry<Integer, String> entry : countryMap.entrySet()){
			System.out.println(entry);
			
		}
		
		Collections.sort(lstCountries);
		System.out.println(lstCountries);
		
		System.out.println("My Most Loved Countries by Rank are:");
		
			
		for (int z = 0; z < countryMap.size(); z++){
		  System.out.println("Rank #" + (z + 1) + " " + countryMap.get(z));
		  
		}
		
	}

}
