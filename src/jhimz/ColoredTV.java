package jhimz;

public class ColoredTV extends Television {

	public static int brightness;
	public static int contrast;
	public static int picture;
	

	public void brightnessUP(){
		if (brightness <= 50){
			brightness++;
		}
	}

	public void brightnessDown(){
		if (brightness > 0){
			brightness--;
		}
	}

	public void contrastUP(){
		if (contrast <=50){
			contrast++;
		}
	}
	
	public void contrastDown(){
		if (contrast > 0){
			contrast--;
		}
	}
	
	public void pictureUP(){
		if (picture <=50){
			picture++;
		}
	}
	
	public void pictureDown(){
		if (picture > 0){
			picture--;
		}
	}
	
	public void switchToChannel(int channel){
		this.channel = channel;
	}
	
	public void mute(){
		this.volume = 0;
	}
	
	public String toString(){
		String oldProperties;
		String newProperties;
		
		oldProperties = brandName + " " + modelName + "[on:" + powerOn + ", channel:" + channel + ", volume:" + volume + "]";
				
		newProperties = " [b:" + brightness + ", c:" + contrast + ", p:" + picture + "]";
		
		return oldProperties + newProperties;
	}
	
	public static void main(String[] args) {
		ColoredTV bnwTV = new ColoredTV();
		
		brightness = 50;
		contrast = 50;
		picture = 50;
		
		bnwTV.brandName = "Admiral";
		bnwTV.modelName = "A1";
			
		System.out.println(bnwTV.toString());
		
		ColoredTV sonyTV = new ColoredTV();
		
		sonyTV.brandName = "SONY";
		sonyTV.modelName = "S1";
		
		sonyTV.brightnessUP();
		
		System.out.println(sonyTV.toString());
		
		ColoredTV sharpTV = new ColoredTV();
		
		sharpTV.brandName = "SHARP";
		sharpTV.modelName = "SH1";
		
		sharpTV.mute();
		sharpTV.brightnessDown();
		sharpTV.contrastUP();
		sharpTV.pictureDown();
		
		System.out.println(sharpTV.toString());

	}
	
	
}
