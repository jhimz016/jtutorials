package jhimz;

public class SmartTV extends ColoredTV {
	
	public String connectToNetwork(String netName){
		String conMessage = "Network Name: " + netName;
		
		return conMessage;
		
	}
	
	public boolean hasNetworkConnection(){
		return true;
	}

	public static void main(String[] args) {
		
		SmartTV samsungTV = new SmartTV();
		
		samsungTV.brandName = "SAMSUNG";
		samsungTV.modelName = "SMTV1";
		
		System.out.println(samsungTV.toString());
		samsungTV.mute();
		
		for (int x=1; x <=3; x++){
			samsungTV.brightnessUP();
		}
		
		for (int x=5; x >=0; x--){
			samsungTV.brightnessDown();
		}
		
		for (int x=1; x <=3; x++){
			samsungTV.contrastUP();
		}
		
		for (int x=5; x >=0; x--){
			samsungTV.contrastDown();
		}

		for (int x=1; x <=3; x++){
			samsungTV.pictureUP();
		}
		
		samsungTV.pictureDown();
		samsungTV.switchToChannel(2);

		System.out.println(samsungTV.toString());
		
		System.out.println(samsungTV.connectToNetwork("TestNetwork"));
		
		
	}

}
