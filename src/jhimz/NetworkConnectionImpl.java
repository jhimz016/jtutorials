package jhimz;

import jhimz.NetworkConnection;

public class NetworkConnectionImpl implements NetworkConnection {
	
	public String connect(String netName){
		String estabConnection = "Network Name: " + netName;
		
		return estabConnection;
	}
	
	public boolean connectionStatus(){
		return true;
	}

	public static void main(String[] args) {
		
	NetworkConnectionImpl newConnection = new NetworkConnectionImpl();
	
	System.out.println(newConnection.connect("Test Network"));
	System.out.println("Is Connected? " + newConnection.connectionStatus());

	}

}
