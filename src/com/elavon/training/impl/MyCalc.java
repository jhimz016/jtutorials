package com.elavon.training.impl;

import com.elavon.training.intf.AwesomeCalculator;

public class MyCalc implements AwesomeCalculator {
	

	@Override
	public int getSum(int augend, int addend) {
		int z;
		
		z = augend + addend;
		return z;
	}

	@Override
	public double getDifference(double minuend, double subtrahend) {
		double z;
		
		z = minuend - subtrahend;
		return z;
	}

	@Override
	public double getProduct(double multiplicand, double multiplier) {
		double z;
		
		z = multiplicand * multiplier;
		return z;
	}

	@Override
	public String getQuotientAndRemainder(int dividend, int divisor) {
		int z;
		int rem;
		
		z = dividend / divisor;
		rem = dividend % divisor;
		
		
		return ("The quotient is " + z + " and the remainder is " + rem);
	}

	@Override
	public double toCelsius(int fahrenheit) {
		int z;
		
		z = (fahrenheit - 30) / 2;
		return z;
	}

	@Override
	public double toFahrenheit(int celsius) {
		double z;
		
		z = (celsius * 1.8) + 32;
		return z;
	}

	@Override
	public double toKilogram(double lbs) {
		double z;
		
		z = lbs / 2.2;
		return z;
	}

	@Override
	public double toPound(double kg) {
		double z;
		
		z = kg * 2.2;
		return z;
	}

	@Override
	public boolean isPalindrome(String str) {
		int var = str.length();
		char txt1;
		char txt2;
		int end;
		boolean result = true;
		
		for (int pos = 0; pos < str.length(); pos++){
			txt1 = str.charAt(pos);
			end = var - (pos + 1);
			txt2 = str.charAt(end);
			
			
			if (txt1 != txt2){result = false;}
		}	
			
		return result;
		
	}		

	public static void main(String[] args) {
		
		int x = 10;
		int y = 3;
		String palindrome = "NASABAYABASAN";
		
		MyCalc calc = new MyCalc();
		
		System.out.println("implementation of AwesomeCalculator.getSum method = " + calc.getSum(x, y));
		System.out.println("implementation of AwesomeCalculator.getDifference method = " + calc.getDifference(x, y));
		System.out.println("implementation of AwesomeCalculator.getProduct method = " + calc.getProduct(x, y));
		System.out.println("implementation of AwesomeCalculator.getQuotientAndRemainder method = " + calc.getQuotientAndRemainder(x, y));
		System.out.println("implementation of AwesomeCalculator.getCelcius method = " + calc.toCelsius(x));
		System.out.println("implementation of AwesomeCalculator.getFahrenheit method = " + calc.toFahrenheit(y));
		System.out.println("implementation of AwesomeCalculator.getKilogram method = " + calc.toKilogram(x));
		System.out.println("implementation of AwesomeCalculator.getPound method = " + calc.toPound(y));
		System.out.println("implementation of AwesomeCalculator.isPalindrome method = " + calc.isPalindrome(palindrome));
		
	}
}
