package com.elavon.training.main;

import com.elavon.training.impl.MyCalc;

public class Main extends MyCalc {
	
	public String message;
	

	
	public static void main(String[] args) {

		int x = 100;
		int y = 30;
		String txt = "EVEREVE";
		
		Main mainCalc = new Main();
		
		System.out.println("Calling the implementation of AwesomeCalculator.getSum " + mainCalc.getSum(x, y));
		System.out.println("Calling the implementation of AwesomeCalculator.getDifference " + mainCalc.getDifference(x, y));
		System.out.println("Calling the implementation of AwesomeCalculator.getProduct " + mainCalc.getProduct(x, y));
		System.out.println("Calling the implementation of AwesomeCalculator.getQuotientAndRemainder " + mainCalc.getQuotientAndRemainder(x, y));
		System.out.println("Calling the implementation of AwesomeCalculator.toKilogram " + mainCalc.toKilogram(x));
		System.out.println("Calling the implementation of AwesomeCalculator.toPound " + mainCalc.toPound(y));
		System.out.println("Calling the implementation of AwesomeCalculator.toCelcius " + mainCalc.toCelsius(x));
		System.out.println("Calling the implementation of AwesomeCalculator.toFahrenheit " + mainCalc.toFahrenheit(y));
		System.out.println("Calling the implementation of AwesomeCalculator.isPalindrome method = " + mainCalc.isPalindrome(txt));
	}

}
