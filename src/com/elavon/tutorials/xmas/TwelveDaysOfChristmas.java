package com.elavon.tutorials.xmas;

public class TwelveDaysOfChristmas {
	
	String days;
	String regalo;
	static boolean bol = false;
	
	public void stanza(int var){
		
		switch (var){
		case 1: days = "first"; break;
		case 2: days = "second"; break;
		case 3: days = "third"; break;
		case 4: days = "fourth"; break;
		case 5: days = "fifth"; break;
		case 6: days = "sixth"; break;
		case 7: days = "seventh"; break;
		case 8: days = "eighth"; break;
		case 9: days = "ninth"; break;
		case 10: days = "tenth"; break;
		case 11: days = "eleventh"; break;
		case 12: days = "twelfth"; break;
		}
		
		System.out.println("On the " + days + " day of Christmas");
		System.out.println("My true love sent to me:");
		
	}
	
	public void gifts(int var){
		switch (var){
		case 1: regalo = "a Partridge in a Pear Tree"; break;
		case 2: regalo = "Two Turtle Doves"; break;
		case 3: regalo = "Three French Hens"; break;
		case 4: regalo = "Four Calling Birds"; break;
		case 5: regalo = "Five Golden Rings"; break;
		case 6: regalo = "Six Geese a Laying"; break;
		case 7: regalo = "Seven Swans a Swimming"; break;
		case 8: regalo = "Eight Maids a Milking"; break;
		case 9: regalo = "Nine Ladies Dancing"; break;
		case 10: regalo = "Ten Lords a Leaping"; break;
		case 11: regalo = "Eleven Pipers Piping"; break;
		case 12: regalo = "Twelve Drummers Drumming"; break;
		}
		
		System.out.println(regalo);
	}
	
	public static void printLyrics(){
		TwelveDaysOfChristmas lyrics = new TwelveDaysOfChristmas();
		
		int x = 1;
		int y;
		
		for (; x < 13; x++){
			lyrics.stanza(x);
			
			y = x;
			
			while (y!=0) {
								
				if (y == 1 && x > 1){
					System.out.print("and ");
				}
					
				
				lyrics.gifts(y);
				
				y--;
				
				

				
			}
			
			System.out.println();
		}

		
	}
	
	public static void main(String[] args) {
		TwelveDaysOfChristmas.printLyrics();
	}

}
